import toast from '@/utils/notify';

const types = {
  error: 'mdi-alert',
  warning: 'mdi-alert-circle-outline',
  info: 'mdi-information-outline',
  success: 'mdi-checkbox-marked-circle-outline',
};

export default {
  install(Vue, options) {
    Vue.prototype.$notify = Object.keys(types).reduce((notifications, key) =>
      Object.assign(
        notifications,
        {
          [key]: (params) => toast({ type: key, icon: types[key], ...params }),
        },
        {}
      )
    );
  },
};
