import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import '@mdi/font/css/materialdesignicons.min.css';
import ru from '@/locales/ru';
import en from '@/locales/en';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: localStorage.getItem('darkMode') || false,
    options: {
      customProperties: true,
    },
    themes: {
      options: {
        themeCache: {
          get: (key) => localStorage.getItem(key),
          set: (key, value) => localStorage.setItem(key, value),
        },
      },
      light: {
        primary: '#F02B2E',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
        commonBar: '#EFF0F2',
        background: '#f2f2f2',
        navbar: {
          background: '#fff',
        },
      },
      dark: {
        commonBar: '#808989',
        primary: '#FF5252',
        background: '#363636',
        navbar: {
          background: '#222222',
        },
      },
    },
  },
  icons: {
    iconfont: 'mdiSvg',
  },
  lang: {
    locales: { ru, en },
    current: localStorage.getItem('lang') || 'ru',
  },
});
