export default class Throttle {
  constructor(name, type, obj) {
    this.running = false;
    this.name = name || 'optimizedResize';
    this.type = type || 'resize';
    this.obj = obj || window;
    this.throttle();
  }

  throttle() {
    this.obj.addEventListener(this.type, this.eventHandler);
  }

  customEventHandler = () => {
    this.obj.dispatchEvent(new CustomEvent(this.name));
    this.running = false;
  };

  eventHandler = () => {
    if (!this.running) {
      this.running = true;
      requestAnimationFrame(this.customEventHandler);
    }
  };
}
