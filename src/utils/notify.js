import iziToast from 'izitoast';
import 'izitoast/dist/css/iziToast.min.css';
import '@/styles/index.scss';
/**
 * Notification component class name
 * @type {string} - css class name
 */
const className = 'v-notification';
/**
 * Create BEM-modification class name from prop
 * @returns {string}
 * @param mod
 */
const setMod = (mod) => {
  const position = mod && mod.toString().match(/^([bottom, top]*)/i);
  return mod && `${className}--${position[0] ? position[0] : mod}`;
};
/**
 * Create from param a BEM class names with modificators for notification components
 * @returns {string}
 * @param params - BEM modifiers
 */
const setClasses = (...params) =>
  params.reduce((name, mod) => name + ` ${setMod(mod)}`, className);
/**
 * Lunch a notification message
 * @param type {string}- notification type
 * @param icon {string}- notification icon (mdi)
 * @param params {
 * title , message, icon, timeout, position, displayMode, class, progressBar,
 * layout, titleColor, iconColor, backgroundColor, progressBarColor...
 * }- other params
 */
export default ({ type, icon, ...params }) => {
  iziToast[type]({
    timeout: 2000,
    position: 'topRight',
    backgroundColor: '#fff',
    displayMode: 0,
    class: setClasses(type, (params.position = 'topRight')),
    iconColor: `inherit`,
    titleColor: `inherit`,
    progressBarColor: `var(--v-${type}-lighten1)`,
    transitionIn: 'fadeInLeft',
    transitionOut: 'fadeOutDown',
    transitionInMobile: 'fadeInUp',
    transitionOutMobile: 'fadeOutDown',
    layout: 2,
    maxWidth: 400,
    icon: `v-icon mdi ${icon}`,
    ...params,
  });
};
