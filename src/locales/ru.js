import ru from 'vuetify/lib/locale/ru';

export default {
  ...ru,
  locale: {
    key: 'РУ',
    changeMsg: 'Язык системы сменен на',
    name: 'Русский',
  },
  navigation: {
    home: 'Главная',
    pages: 'Страницы',
    articles: 'Статьи',
    documents: 'Документы',
    offices: 'Офисы',
    contacts: 'Контакты',
    users: 'Пользователи',
    settings: 'Настройки',
  },
  theme: {
    on: 'Включить темную тему',
    off: 'Выключить темную тему',
    name: 'Темная тема',
    changeMsg: 'Тема переключена!',
  },
  logout: 'Выйти',
};
