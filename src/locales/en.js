import en from 'vuetify/lib/locale/en';

export default {
  ...en,
  locale: {
    key: 'EN',
    changeMsg: 'Current language switched to',
    name: 'English',
  },
  navigation: {
    home: 'Home',
    pages: 'Pages',
    articles: 'Articles',
    documents: 'Documents',
    offices: 'Offices',
    contacts: 'Contacts',
    users: 'Users',
    settings: 'Settings',
  },
  theme: {
    on: 'Turn on dark mode',
    off: 'Turn off dark mode',
    name: 'Dark mode',
    changeMsg: 'Theme was changed!',
  },
  logout: 'Logout',
};
