import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters';
import modules from './tools/autoImport';
Vue.use(Vuex);

const store = new Vuex.Store({
  modules,
  getters,
  strict: process.env.NODE_ENV !== 'production',
});

export default store;
