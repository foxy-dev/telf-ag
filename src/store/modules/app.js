const state = {
  device: 'desktop',
  loading: true,
  error: false,
};

const mutations = {
  APP_DEVICE: (state, device) => {
    state.device = device;
  },
  APP_LOAD: (state) => {
    state.loading = true;
  },
  APP_SUCCESS: (state) => {
    state.loading = false;
    state.error = false;
  },
  APP_ERROR: (state) => {
    state.loading = false;
    state.error = true;
  },
};

const actions = {
  toggleDevice({ commit }, device) {
    commit('APP_DEVICE', device);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
