import ApiClientClass from '@/api/utils/ApiClientClass';

/**
 * Simple RESTful resource class
 */
export default class Resource extends ApiClientClass {
  constructor(uri) {
    super();
    this.uri = '/' + uri;
  }
  list(query) {
    return this.client({
      url: this.uri,
      method: 'get',
      params: query,
    });
  }
  get(id) {
    return this.client({
      url: this.uri + '/' + id,
      method: 'get',
    });
  }
  store(resource) {
    return this.client({
      url: this.uri,
      method: 'post',
      data: resource,
    });
  }
  update(id, resource) {
    return this.client({
      url: this.uri + '/' + id,
      method: 'put',
      data: resource,
    });
  }
  destroy(id) {
    return this.client({
      url: this.uri + '/' + id,
      method: 'delete',
    });
  }
}
