import axios from 'axios';
import toast from '../../utils/notify';
// Create axios instance
export default class ApiClientClass {
  constructor(options = {}) {
    this.client = axios.create({
      baseURL: process.env.VUE_APP_API_URL,
      timeout: options.timeout || 10000,
    });

    this.client.interceptors.request.use(
      (config) => {
        return config;
      },
      (e) => Promise.reject(e)
    );

    this.client.interceptors.response.use(
      (response) => {
        return response.data;
      },
      (e) => {
        let message = e.message;
        if (e.response.data && e.response.data.errors) {
          message = e.response.data.errors;
        } else if (e.response.data && e.response.data.error) {
          message = e.response.data.error;
        }

        toast({
          message: message,
          title: 'Ошибка!',
          type: 'error',
          timeout: 5 * 1000,
        });

        return Promise.reject(e);
      }
    );
  }
}
