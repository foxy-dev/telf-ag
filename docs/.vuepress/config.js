module.exports = {
    base: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/docs/'
    : '/docs/',
    title: 'ADMIN PANEL DOCS | TELF AG',
    description: 'Just playing around',
    themeConfig: {
        docsDir: '/docs/',
        smoothScroll: true,
        lastUpdated: 'Last Updated',
        editLinks: true,
        sidebarDepth: 2,
        sidebar: {
          '/guides/': [{
            title: 'The Feathers guide',
            collapsable: true,
            children: [
              ['Introduction.md', 'Introduction'],
              'page1.md',
              'page2.md',
              'page3.md',
            ]
          }]
        },
        nav: [
          { text: 'Home', link: '/' },
          { text: 'Guide', link: '/guides/' },
          { text: 'External', link: 'https://google.com' }
        ]
      },
  }
