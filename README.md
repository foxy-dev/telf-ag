# telf-admin

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Build docs
```
yarn docs:build
```

### Compiles and hot-reloads for docs development
```
yarn docs:dev
```
